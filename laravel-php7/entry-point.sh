#!/usr/bin/env sh

case "$1" in
"start-fpm")
  # Rewrite SIGINT  to SIGQUIT
  # Rewrite SIGTERM to SIGQUIT
  # Rewrite SIGHUP  to SIGUSR2
  dumb-init --rewrite 2:3 --rewrite 15:3 --rewrite 1:17 \
    php-fpm7 --force-stderr -Fc /etc/php7/php-fpm.conf
  ;;

"start-nginx")
  sed -i "s/__FASTSGI_ADDR/${FASTSGI_ADDR:-127.0.0.1:9000}/g" /etc/nginx/http.d/default.conf
  nginx -g 'daemon off;'
  ;;

*)
  if [ "$*" = '' ]; then
    echo "There is no targets to run"
    exit 1
  fi

  echo Executing "$@"
  # shellcheck disable=SC2068
  $@
  ;;
esac
