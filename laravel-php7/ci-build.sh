#!/usr/bin/env sh

set -xe
# shellcheck disable=SC2046
cd $(dirname "$0")

docker pull "$CI_REGISTRY_IMAGE/laravel-php7:latest" || true

docker build . --pull \
  --cache-from "$CI_REGISTRY_IMAGE/laravel-php7:latest" \
  --tag "$CI_REGISTRY_IMAGE/laravel-php7:$CI_COMMIT_SHORT_SHA" \
  --tag "$CI_REGISTRY_IMAGE/laravel-php7:latest" \
  --tag "$CI_REGISTRY_IMAGE/laravel:php7" # legacy

docker push "$CI_REGISTRY_IMAGE/laravel-php7:$CI_COMMIT_SHORT_SHA"
docker push "$CI_REGISTRY_IMAGE/laravel-php7:latest"
docker push "$CI_REGISTRY_IMAGE/laravel:php7" # legacy
